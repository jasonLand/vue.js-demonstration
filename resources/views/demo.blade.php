@extends('layouts.app')
@section('head')
@endsection
@section('content')
    <div id="vueapp" class="container">
        <app pagecount="{{ $pageCount }}"></app>
    </div>
@endsection
@section('footer')
@endsection
@section('foot')
    {{-- Pull in the browserified hunk of js gulp creates --}}
    <script src="{{ URL::to('js/demo.js') }}"></script>
@endsection