@extends('layouts.app')

@section('head')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{--<div class="jumbotron">--}}
                {{--<h1>Jumbotron</h1>--}}
            {{--</div>--}}
            <exampleEditor example="{{ json_encode($example) }}"></exampleEditor>
        </div>
    </div>
</div>
@endsection
@section('footer')
@endsection
@section('foot')
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/ace.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/mode-markdown.js"></script>
    <script src="{{ URL::to('js/main.js') }}"></script>
@endsection
