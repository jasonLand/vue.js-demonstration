# Welcome to Vue.js

Vue.js is a reactive javascript framework that competes with the likes of;

* Angular
* React
* Riot
* Ember
* Polymer

It does all that fancy binding, isolates scope sensibly and has a relatively shallow learning curve compared to the likes of Angular or Polymer.

## To follow this guide just run the following (assuming you've cloned this repo)

1.  Switch to the demo branch with `git checkout demo` and perhaps a `git pull demo` just to be sure you've got everything

2.  Begin stepping through with `git checkout step0`  (there are steps up to 25)

3.  If it complains you've made changes that need to be committed or stashed in between changes, just run `git stash` and then checkout the next step

4.  Navigate to /projectDirectory/public/demo to begin

5. Enjoy

# Step 1
Basic data binding

Standard binding with {{ varName }} as long as that chunk of html is being controlled by Vue.

!! Show basic vue instance setup !!

!! Explain laravel blade @{{ prefix !!

# Step 2
Binding is 2-way (by default, if you want)

!! Show v-model on input !!

# Step 3
Rendering a list with v-for

!! Show how the data field works, show alternate funciton return method so data doesn't end up reused when not expected !!

# Step 4
Uppercase filter

!! Show filter and pipe !!

# Step 5
Lowercase filter

!! Show filter and pipe !!

# Step 6
ucfirst functionality (on entire string, not per-word)

# Step 7
json filter for displaying data

!! Explain how it's useful for debugging !!

# Step 8
Filters can have arguments, this one modifies the indentation of each level of the json.

!! Arguments are space-separated after the filter name !!

# Step 9
We've been using v-for but how can we control it?  With filters!  orderBy

!! Show how it's "orderBy 'fieldName' optional order inversion" !!

# Step 10
Another filter for v-for called filterBy

!! Show how it's filtering on capital C in the field 'name', switch this up as a demo !!

# Step 11
Custom components time!
We can create our own tags
`<customtag></customtag>`
Just like in other frameworks.

!! Show how we can define the template and then use hook it up to the component !!

!! Explain why I added data !!

# Step 12
Simplified component definition

!! Explain component definition flexiblity.!!

!! Copy below in for template tag method !!
```
<template id="personcomponent">
    <div>I'm a Person Component</div>
</template>
```

# Step 13
Now the component is actually doing something.

!!Show how it defines "props" and how that gets sucked in.!!

!! Show how this could also be v-bind:person !!

# Step 14
Computed properties are helpful if you need to transform some data between the model and the view.

!! Show the salary computed property function.  Show that we're also applying the 'currency' filter so it formats it nicely !!  `"£"`

!! Maybe demonstrate VUE devtools chrome extension !!

# Step 15
v-model binding is dynamic, even for computed properties

!! Show how all the data changes, even the root data object !!

!! Remark that computed properties are getter-only by default but a set function can be defined to make them two-way (splitting first and last names out of one input field) !!

# Step 16
Methods are local to the component they are defined on.  This is pretty handy for segregating functionality by components.  You define them similarly to computed properties.

!! Show remove function, also remark how I had to to track the index (provided by default on v-for as $index) and traverse to the parent and snip the person out of the parent's array using the index !!

# Step 17
Let's build an app.  And let's do it sensibly.  I'm going to use the following to accomplish this;
* Gulp
* Browserify via npm
* Vueify via npm automagically hooks into browserify
* Separate app and component .vue files for sanity

This means we'll run a gulp task (default gulp task in this case) whenever we change a .vue file so it is bundled up with all the depenedencies and mashed into /js/demo.js

!! Explain the example.io idea and that we'll just be showing example cards with comments and pagination as an end goal !!

# Step 18
To set this up we'll add an `<app></app>` tag into our html as well as pull in js/demo.js.  In our HTML this is almost all we'll have to do from here on out (almost)!

In demo.js I'll require Vue and a dependency we'll use in the next step.  We'll also create /demoComponents/app.vue and require that as well.

Finally we'll reference our app.vue require as a component so our `<app></app>` tag will come alive.

!! Show off all new files, especially the .vue file structure !!

Things are going to accelerate at this point.  Hold on tight.

# Step 19
We'll use vue-resource to ajax in the data from a laravel route that is serving json

!! Show that we do this in the app.vue file, show that we're echoing the json to the view via the json filter !!

!! Quick overview of our example data !!

# Step 20
Now we have an example component and we're passing in single example's-worth of data.  Notice we're passing it in as a property inside of a v-for.  This means we'll now have an example.vue file.

!! Show that we are requiring the example.vue file inside the app.vue file and adding it to the components object for the app component. !!

!! Show off the example.vue file.  Note the template is just a bootstrap panel an we're referencing a lot of the data in our example object.  Note the example vue definition is very minimal (just a props definition) !!

Note we're not rendering the markdown yet.

# Step 21
Now we're going to render this markdown.  Since the only thing that needs to render markdown is the example component this is where we'll require that dependency (a package called "marked").

!! Show how I require marked, set the github flavored markdown option to true and set marked up as a filter (since filters just pass a value into a function and expect a return) !!

Voila

# Step 22
Now that we have a handle on this nested components thing let's nest another component.  Comments!  You may notice we had a comments object nested inside our example object, we'll be using that.

This means a new comments.vue file, requiring the file in the example component and adding the component to the example component.

There's a lot more going on in this one.  We have a click handler on a button to show/hide the comments secction (@click).  We have a computed property that will be bound to the text on the button so it toggles as we toggle the comments field.

!! Show how we're not doing any .toggle() style jquery stuff.  That's oldschool.  We want our view to reflect our model.  So if we want a toggleable comments section we should have a piece of data that reflects this state.  That piece of data is called showHide and it defaults to false. !!

!! Note the v-if that will throw a message to the view if there are no comments to view !!

# Step 23
Let's make it so we can show a pageful of examples and expand the ones we want to view.  We'll get rid of our json output because it is cluttering our view at this point and we get what's going on.  We will follow a similar patter with the show/hide toggling that we used on the comments section.  I want to pass in a boolean as a property to the `<example></example>` tag so I will need to do some validation on this incoming property.  If I don't a true or a false in that attribute will come through as a string.  I had a typo in this step, I'm fixing that below.  It still worked but for all the wrong reasons, it would have caused issues downt he road.
```
            showhide: {
                type: Boolean,
                coerce: function(val){
                    return "true" == val;
                },
                default: true
            }
```

!! Make a note of the showHideGlyph computed property, handy for updating the class on the glyphicon.  Also note I'm hiding the comments button by traversing to the $parent object and seeing if the parent is shown/hidden !!

# Step 24
Pagination is fun, isn't it?  I'm going to use laravel to report the number of pages I should expect and stick that into a property attribute.  I'll validate this as an integer and use it to limit my pagination links.  I've also created a `<pagelinks></pagelinks>` component.

I do some simple math to figure out my bounds and then I render the view based on these bounds.

Note:  I'm actually using the `<style></style>` block inside pagelinks.vue.

I'll dispatch an event up the chain (broadcast is the term for down the chain) and this will be caught by the `<app></app>` component.  The app component catches this because it does all the ajaxing and handling of the main examples data payload.  Everything then flows down into the child components to render the view.

!! Show off the computed properties and how they're used to generate the view !!

!! Walk everyone through the event and how I'm grabbing new data based on this !!

# Step 25
First and Last page links.  Just adding another conditional set of page links that will go to either the first or the last page.  A little css in the `<style></style>` block is used to accomplish this.

Voila, we have the viewing portion of our example.io site.

# POST DEMONSTRATION EXAM
### Q: How many times did I misspell "example"?